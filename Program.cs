using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV5
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IShipable> ListOfProducts = new List<IShipable>();
            Box box1 = new Box("Box num1");
            ListOfProducts.Add(new Product("Product num1", 10, 1));
            ListOfProducts.Add(new Product("Product num2", 12, 3));
            box1.Add(new Box("Box num1"));
            foreach (IShipable item in ListOfProducts)
            {
                box1.Add(item);
            }
            ShippingService service = new ShippingService(2.8);
            double totalPrice = box1.Price + service.PriceOfDelivery(box1);
    
            Console.WriteLine("Total weight is: " + box1.Weight + ", " + "total price is: " + totalPrice + ".");
        }
    }
}
