using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV5
{
    class ShippingService
    {
        private double priceOfMassUnit;
        public ShippingService(double priceOfMassUnit)
        {
            this.priceOfMassUnit = priceOfMassUnit;
        }
        public double PriceOfDelivery(Box item)
        {
            double priceOfDelivery = 0;
            priceOfDelivery = item.Weight * priceOfMassUnit;
            return priceOfDelivery;
        }
    }
}