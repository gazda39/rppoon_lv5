using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV5
{
    interface IShipable
    {
        double Price { get; }
        string Description(int depth = 0);
        double Weight { get; }
    }
}

